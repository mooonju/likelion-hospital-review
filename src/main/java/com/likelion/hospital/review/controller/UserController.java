package com.likelion.hospital.review.controller;

import com.likelion.hospital.review.domain.Response;
import com.likelion.hospital.review.domain.dto.UserDto;
import com.likelion.hospital.review.domain.dto.UserJoinRequest;
import com.likelion.hospital.review.domain.dto.UserJoinResponse;
import com.likelion.hospital.review.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest userJoinRequest) {
        UserJoinResponse joinResponse = userService.join(userJoinRequest);
        return Response.success(joinResponse);
    }
}
