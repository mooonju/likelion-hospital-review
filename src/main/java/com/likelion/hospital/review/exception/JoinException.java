package com.likelion.hospital.review.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class JoinException extends RuntimeException {

	private ErrorCode errorCode;

}
