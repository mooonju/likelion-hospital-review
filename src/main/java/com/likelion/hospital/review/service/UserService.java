package com.likelion.hospital.review.service;

import com.likelion.hospital.review.domain.User;
import com.likelion.hospital.review.domain.dto.UserDto;
import com.likelion.hospital.review.domain.dto.UserJoinRequest;
import com.likelion.hospital.review.domain.dto.UserJoinResponse;
import com.likelion.hospital.review.exception.ErrorCode;
import com.likelion.hospital.review.exception.JoinException;
import com.likelion.hospital.review.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public UserJoinResponse join(UserJoinRequest joinRequest) {
        userRepository.findByUserName(joinRequest.getUserName()).ifPresent((sth) -> {
            throw new JoinException(ErrorCode.DUPLICATE_ID);
        });

        User savedUser = userRepository.save(joinRequest.toEntity());
        return UserJoinResponse.fromEntity(savedUser);
    }
}
