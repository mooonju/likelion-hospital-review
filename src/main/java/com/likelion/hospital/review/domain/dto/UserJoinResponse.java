package com.likelion.hospital.review.domain.dto;

import com.likelion.hospital.review.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserJoinResponse {
    private String userName;
    private String emailAddress;

    public static UserJoinResponse fromEntity(User user) {
        return new UserJoinResponse(user.getUserName(), user.getEmailAddress());
    }
}
